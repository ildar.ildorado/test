<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_statistics".
 *
 * @property int $id
 * @property string $transfer_info
 * @property string $username
 * @property int $user_id
 * @property int $amount
 * @property int $created_at
 *
 * @property User $user
 */
class UserStatistics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_statistics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transfer_info', 'user_id', 'created_at', 'username', 'amount'], 'required'],
            [['user_id', 'created_at'], 'integer'],
            [['transfer_info', 'username'], 'string', 'max' => 255],
            [['amount'], 'number'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transfer_info' => 'Transfer Info',
            'user_id' => 'User ID',
            'amount' => 'Amount',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function updateUsers() {
        if ($this -> isNewRecord) {
            $this -> user_id = Yii::$app -> user -> identity -> id;
            $this -> created_at = time();
//      date('m/d/Y h:i:s a', time());
            $user = $this->findModel($this -> user_id);
            $receiver = User::findOne(["username" => $this -> username]);
            if ($receiver == null) {
                Yii::$app -> session -> setFlash("error", "User doesn't exist!");
                return false;
            }
            if ($user -> username == $this -> username) {
                Yii::$app -> session -> setFlash("error", "You can't transfer to yourself.");
                return false;
            }
            if ($user -> sum - $this -> amount < -1000) {
                Yii::$app -> session -> setFlash("error", "Your can't do that, your balance will be less than -1000.");
                return false;
            }
            $user -> sum = $user -> sum - $this -> amount;
            $receiver -> sum = $receiver -> sum + $this -> amount;
            $user -> save();
            $receiver -> save();
            return true;
        }
    }
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
