<?php

use yii\db\Migration;

/**
 * Class m181003_110351_remove_unused_user_fields
 */
class m181003_110351_remove_unused_user_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{user}}', 'created_at');
        $this->dropColumn('{{user}}', 'updated_at');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this -> addColumn('{{user}}', 'created_at', $this -> integer()->notNull()-> after('sum'));
        $this -> addColumn('{{user}}', 'updated_at', $this -> integer()->notNull()-> after('sum'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181003_110351_remove_unused_user_fields cannot be reverted.\n";

        return false;
    }
    */
}
