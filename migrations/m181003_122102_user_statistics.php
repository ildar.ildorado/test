<?php

use yii\db\Migration;

/**
 * Class m181003_122102_user_operations
 */
class m181003_122102_user_statistics extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_statistics}}', [
            'id' => $this->primaryKey(),
            'transfer_info' => $this->string()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'username' => $this->string(100)->notNull(),
            'amount' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        // creates index for column `project_id`
        $this->createIndex(
            'idx-user_statistics_user_id',
            'user_statistics',
            'user_id'
        );

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-user-project_id',
            'user_statistics',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181003_122102_user_operations cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181003_122102_user_operations cannot be reverted.\n";

        return false;
    }
    */
}
