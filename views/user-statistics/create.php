<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserStatistics */

$this->title = 'Create User Statistics';
$this->params['breadcrumbs'][] = ['label' => 'User Statistics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-statistics-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
