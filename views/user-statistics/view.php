<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserStatistics */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Statistics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-statistics-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'transfer_info',
            'user_id',
            'created_at',
        ],
    ]) ?>

</div>
