<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserStatisticsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Statistics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-statistics-index">

    <p>
        <?= Html::a('transfer money', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'amount',
            'transfer_info',
            'username',
            'created_at',
//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
